from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen


class FirstScreen(Screen):
    def search_for_image(self):
        return None


class RootWidget(ScreenManager):
    pass


class MainApp(App):
    def __build__(self):
        return RootWidget()


MainApp().run()
