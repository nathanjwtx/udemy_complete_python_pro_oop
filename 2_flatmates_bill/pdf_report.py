import webbrowser
import os

from fpdf import FPDF


class PdfReport:
    """
    Creates a pdf of the itemised bill by flatmate
    """

    def __init__(self, filename):
        self.filename = filename

    def generate(self, flatmate1, flatmate2, bill):
        pdf = FPDF(orientation="P", unit="pt", format="A4")
        pdf.add_page()

        # Add icon
        pdf.image("files/house.png", w=30, h=30)

        # Insert title
        pdf.set_font(family="Arial", size=24, style="B")
        pdf.cell(w=0, h=80, txt="Flatmates Bill", border=0, align="C", ln=1)

        # Insert period label and value
        pdf.set_font(family="Arial", size=14, style="B")
        pdf.cell(w=100, h=40, txt="Period: ", border=0)
        pdf.set_font(family="Arial", size=14, style="BI")
        pdf.cell(w=150, h=40, txt=bill.period, border=0, ln=1, align="R")

        # Flatmate 1
        pdf.set_font(family="Arial", size=14)
        pdf.cell(w=100, h=40, txt=flatmate1.name, border=0)
        pdf.cell(
            w=150,
            h=40,
            txt=str(f"${flatmate1.pays(bill, flatmate2)}"),
            border=0,
            ln=1,
            align="R",
        )

        # Flatmate 2
        pdf.cell(w=100, h=40, txt=flatmate2.name, border=0)
        pdf.cell(
            w=150,
            h=40,
            txt=str(f"${flatmate2.pays(bill, flatmate1)}"),
            border=0,
            ln=1,
            align="R",
        )

        os.chdir("files")
        pdf.output(self.filename)
        # opens the file automatically in the default pdf viewer
        webbrowser.open(self.filename)
