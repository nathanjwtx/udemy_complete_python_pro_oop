from bill import Bill
from flatmate import Flatmate
from pdf_report import PdfReport
from file_sharer import FileSharer

f1_name = input("Enter the name of first flatmate...")
f1_days = input(f"Enter the number of days {f1_name} was in the flat...")
f1 = Flatmate(f1_name, int(f1_days))

f2_name = input("Enter the name of second flatmate...")
f2_days = input(f"Enter the number of days {f2_name} was in the flat...")
f2 = Flatmate(f2_name, int(f2_days))

amount = input("Enter the monthly bill amount...")
period = input("Enter the billing period...")
bill = Bill(int(amount), period)

print(f1.pays(bill, f2))

report = PdfReport(f"{period}.pdf")
report.generate(f1, f2, bill)

file_sharer = FileSharer(filepath=report.filename)
file_sharer.share()
