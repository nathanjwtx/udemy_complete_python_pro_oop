class Bill:
    """
    Object contains info regarding flatmate's bill
    """

    def __init__(self, amount, period):
        self.amount = amount
        self.period = period
