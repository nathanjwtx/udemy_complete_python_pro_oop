# from canvas import Canvas


class Rectangle:
    def __init__(self, pos_x, pos_y, width, height, color):  # , canvas):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.width = width
        self.height = height
        self.color = color

    def draw(self, canvas):
        # print(canvas.data)
        canvas.data[
            self.pos_x : self.pos_x + self.width, self.pos_y : self.pos_y + self.height
        ] = self.color
