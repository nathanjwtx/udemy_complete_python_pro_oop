import numpy as np
from PIL import Image

# create 3d numpy array of zeros, then replace zeros (block pixels) with yellow pixels
data = np.zeros((5, 4, 3), dtype=np.uint8)
data[:] = [255, 255, 0]
data[1:3] = [255, 0, 0]
data[4:5] = [0, 255, 0]
print(data)

img = Image.fromarray(data, "RGB")
img.save("canvas.png")
