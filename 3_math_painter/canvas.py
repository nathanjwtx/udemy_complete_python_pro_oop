import numpy as np
from PIL import Image
from os import chdir


class Canvas:
    def __init__(self, x, y, color):
        self.x = x
        self.y = y
        self.color = color
        self.data = np.zeros((self.x, self.y, 3), dtype=np.uint8)
        # updates every value in the array to self.color
        self.data[:] = self.color

    def make_canvas(self, imagepath):
        chdir("images")
        img = Image.fromarray(self.data, "RGB")
        img.save(imagepath)
